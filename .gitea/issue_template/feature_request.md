---

name: "Feature request"
about: "Improve or add a new feature to the software"
title: "A clear and short title"
ref: "main"
labels:
- "Kind: Feature"
- "Status: Stale"
- "Priority: Low"

---

### Before opening:
<!--
Before opening, please ensure that this issue is not duplicated.
-->

- [ ] This issue is not duplicated

### Feature description
<!--
What will this feature do? Why do you want it to be added to TBlock?
-->
