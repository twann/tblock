---

title: "A clear and short title"
ref: "main"
labels:
- "Status: Stale"

---

### Before opening:
<!--
Before opening, please ensure that this PR is not duplicated.
-->

- [ ] This PR is not duplicated
- [ ] I have read [the contributing guidelines](https://codeberg.org/tblock/tblock/src/branch/main/CONTRIBUTING.md).
- [ ] I am OK with being mentioned [as a contributor](https://codeberg.org/tblock/tblock/src/branch/main/CONTRIBUTORS.md)

### About this pull request
<!--
Please check the right choice about your PR. If your pull request, closes an issue, please
specify its reference
-->

- [ ] This pull request closes the issue: #0
- [ ] Other (please specify below)


### Pull request description
<!--
Please describe precisely what this pull request does and why you think it is needed.
-->

