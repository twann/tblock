# Contributing

Your contributions to TBlock are welcome and appreciated. Since we don't want to lock our users and contributors on a single forge platform, we accept contributions and bug reports from the following sources:

- The pull/issue tracker on Codeberg
- Via email
- Our Matrix room
- Our XMPP room
- A direct message to our Mastodon profile

Before reporting an issue, please ensure that it hasn't already been reported in the Codeberg issue tracker. If it has, then you are welcome to join the discussion.

If you want to suggest a change in the code, please do so by opening a pull request or submitting a patch (either by mail, Matrix or XMPP). Also remember that reading [the documentation](https://tblock.codeberg.page/docs) is highly recommended before submitting a change.

## Limitations

We **do not** accept contributions that are:

- Harmful to the project or its user's security or privacy
- Incompatible due to license terms
- Made by individuals who promote discriminatory ideologies such as racism, sexism, homophobia, transphobia, validism, xenophobia, white supremacism or national socialism. We want our community to remain a safe place for people.

## Where to contribute

Currently, we could use help on the following things to do:

- Adding support for init systems such as s6, dinit, etc.
- Beta testing
- Feedback from macOS users
- Fixing the open issues

Soon, we'll also need help translating TBlock and its website/documentation.

## After contributing

First of all, thank you very much, your help is probably really useful! Now, if you want, you can be added to the [list of contributors](https://codeberg.org/tblock/tblock/src/branch/main/CONTRIBUTORS.md).
